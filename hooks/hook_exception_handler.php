<?php

declare(strict_types=1);

use SimpleSAML\Configuration;
use SimpleSAML\Error\CriticalConfigurationError;
use SimpleSAML\Session;

/**
 * Implements HOOK_hook_exception_handler().
 */
function sentry_hook_exception_handler($exception): void
{
    // SimpleSAMLphp is badly broken. There is no reliable way to get
    // Sentry configuration.
    if ($exception instanceof CriticalConfigurationError) {
        return;
    }

    $config = Configuration::getConfig();
    $sentryConfig = Configuration::getConfig('module_sentry.php');

    // See https://docs.sentry.io/platforms/php/configuration/options/ .
    $option_keys = [
        'dsn',
        'environment',
        'release',
        'error_types',
        'sample_rate',
        'max_breadcrumbs',
        'attach_stacktrace',
        'send_default_pii',
        'server_name',
        'in_app_include',
        'in_app_exclude',
        'max_request_body_size',
        'max_value_length',
        'context_lines',
        // See https://github.com/getsentry/sentry-php/blob/master/UPGRADE-4.0.md
        'ignore_exceptions',
        // Hooks.
        'before_send',
        'before_send_transaction',
        'before_breadcrumb',
        // Transport Options.
        'transport',
        'http_connect_timeout',
        'http_timeout',
        // Tracing Options.
        'enable_tracing',
        'traces_sample_rate',
        'traces_sampler',
    ];
    $options = [];
    // Pass through all the options.
    foreach ($option_keys as $key) {
        if ($sentryConfig->hasValue($key)) {
            $options[$key] = $sentryConfig->getValue($key);
        }
    }
    if ($config->hasValue('proxy')) {
        $options['http_proxy'] = $config->getValue('proxy');
    }

    \Sentry\init($options);
    $session = Session::getSessionFromRequest();
    if ($session) {
        \Sentry\configureScope(function (\Sentry\State\Scope $scope) use ($session): void {
            $scope->setExtra('simpleSAMLphp.trackId', $session->getTrackID());
        });
    }

    \Sentry\captureException($exception);
}
